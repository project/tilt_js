INTRODUCTION
------------

Tilt JS is a tiny requestAnimationFrame powered 60+fps lightweight parallax
hover tilt effect for jQuery.

The Tilt JS module is a Libraries API module, for integrating the Javascript
library, Tilt JS.

There is also a sub-module for directly applying Tilt JS
effects to blocks directly through the UI. This module is called Tilt JS Blocks.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/alxs/2855990
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2855990?categories=All


REQUIREMENTS
------------

This module requires the following modules:

 * Libraries (https://drupal.org/project/libraries)
 * jQuery Update (https://www.drupal.org/project/jquery_update)


RECOMMENDED MODULES
-------------------

 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.


INSTALLATION
------------

 * Download the module and copy it into your contributed modules folder:
   [for example, your_drupal_path/sites/all/modules] and enable it.
 * Download the Tilt JS jQuery files from
   https://github.com/gijsroge/tilt.js/archive/master.zip
 * Unzip the folder and copy the contents to either `sites/all/libraries/tiltjs`
   or to a site-specific libraries folder such as
   `sites/example.com/libraries/tiltjs`.
 * You can remove everything from the package apart from the `dest` folder and
   it's contents and the `package.json` file, as the module uses this to detect
   the installed version.


CONFIGURATION
-------------

 * Configure library version in
   Administration » Configuration » Media » Tilt JS


TROUBLESHOOTING
---------------

 * If the library does not load, please check that the library files are in the
   correct location, as referenced in the install instructions above.


MAINTAINERS
-----------

Current maintainers:

 * Alex Scott (djalxs) - https://drupal.org/user/djalxs
