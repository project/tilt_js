(function ($) {
  'use strict';

  /**
   * Provide the summary information for the block settings vertical tabs.
   */
  Drupal.behaviors.tiltJsBlocksAdmin = {
    attach: function (context) {
      // The drupalSetSummary method required for this behavior is not available
      // on the Blocks administration page, so we need to make sure this
      // behavior is processed only if drupalSetSummary is defined.
      if (typeof jQuery.fn.drupalSetSummary === 'undefined') {
        return;
      }

      $('fieldset#edit-tilt-js--2', context).drupalSetSummary(function (context) {
        var $radio = $('input[name="tilt_js[tilt_js_enabled]"]:checked', context);

        if (typeof $radio.val() !== 'undefined') {
          return Drupal.t('Tilt JS enabled');
        }
        else {
          return Drupal.t('Tilt JS disabled');
        }
      });
    }
  };

})(jQuery);
